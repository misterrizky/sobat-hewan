function validasi1(){
    var checkRadio = document.querySelector('input[name="radio1"]:checked');
    if(checkRadio == null) {
        document.getElementById("alert-check-radio1").innerHTML = "Silahkan pilih salah satu pilihan yang ada"; 
        return false;
    } 
    else { 
        document.getElementById("alert-check-radio1").innerHTML = "";
    }

    var checks = document.getElementsByName('check1');
    var hasChecked = 0;
    for(var i = 0; i < checks.length; i++){
        if (checks[i].checked){
            hasChecked++;
        }
    }
    if (hasChecked < 1){
        document.getElementById("alert-checkbox1").innerHTML = "Checkbox tidak boleh kosong.";
        return false;
    }
    else{
        document.getElementById("alert-checkbox1").innerHTML = "";
    }

    var simbol = /[0-9.!#$%&'*+/=?^_`{|}~-]/;
    var NamaHewan = document.getElementById("nama-hewan");
    if(NamaHewan.value.length == 0){
        document.getElementById("alert-nama-hewan").innerHTML = "Nama hewan tidak boleh kosong."
        NamaHewan.focus();
        return false;
    }
    else if(NamaHewan.value.match(simbol)){
        document.getElementById("alert-nama-hewan").innerHTML = "Nama hewan tidak boleh memiliki karakter selain huruf."
        NamaHewan.focus();
        return false;
    }
    else{
        document.getElementById("alert-nama-hewan").innerHTML = "";
    }

    var DeskripsiHewan = document.getElementById("deskripsi-hewan");
    if(DeskripsiHewan.value == ''){
        document.getElementById("alert-deskripsi-hewan").innerHTML = "Deskripsi hewan tidak boleh kosong";
        DeskripsiHewan.focus();
        return false;
    }
    else{
        document.getElementById("alert-deskripsi-hewan").innerHTML = "";
    }
}

function validasi2(){
    var checkRadio = document.querySelector('input[name="radio2"]:checked');
    if(checkRadio == null) {
        document.getElementById("alert-check-radio2").innerHTML = "Silahkan pilih salah satu pilihan yang ada.";
        return false;
    } 
    else { 
        document.getElementById("alert-check-radio2").innerHTML = "";
    }

    var NominalPembayaran = document.getElementById("nominal-pembayaran");
    if(NominalPembayaran.value < 5000){
        document.getElementById("alert-nominal-pembayaran").innerHTML = "Minimal jumlah donasi adalah Rp. 5000,-";
        return false;
    }
    else{
        document.getElementById("alert-nominal-pembayaran").innerHTML = "";
    }
}