function validasi1(){
    var nama = document.getElementById("nama");
    var userName = document.getElementById("username");
    var passWord = document.getElementById("password");
    var passwordUlang = document.getElementById("passwordUlang");
    var tanggalLahir = document.getElementById("tanggalLahir");
    var noTelp = document.getElementById("noTelp");
    var email = document.getElementById("email");
    var kabupaten = document.getElementById("kabupaten");
    var provinsi = document.getElementById("provinsi");
    var alamat = document.getElementById("alamat");
    if(nama.value.length == 0){
        document.getElementById("alert-nama").innerHTML = "Harap isi nama lengkap anda"
        nama.focus();
        return false;
    }
    else{
        document.getElementById("alert-nama").innerHTML = "";
    }
    if(userName.value.length == 0){
        document.getElementById("alert-username").innerHTML = "Harap isi username anda"
        userName.focus();
        return false;
    }
    else{
        document.getElementById("alert-username").innerHTML ="";
    }
    if(passWord.value.length == 0){
        document.getElementById("alert-password").innerHTML = "Harap isi password anda"
        passWord.focus();
        return false;
    }
    else{
        document.getElementById("alert-password").innerHTML ="";
    }
    if(passwordUlang.value.length == 0){
        document.getElementById("alert-passwordUlang").innerHTML = "Harap isi ulang password anda"
        passwordUlang.focus();
        return false;
    }
    else if (passWord.value != passwordUlang.value){
        document.getElementById("alert-passwordUlang").innerHTML = "Password tidak sama";
        passwordUlang.focus();
        return false;
    }
    else{
        document.getElementById("alert-password").innerHTML ="";
    }
    if(tanggalLahir.value.length == 0){
        document.getElementById("alert-tanggalLahir").innerHTML = "Harap masukan tanggal lahir anda"
        tanggalLahir.focus();
        return false;
    }
    else{
        document.getElementById("alert-tanggalLahir").innerHTML ="";
    }
    if(noTelp.value.length == 0){
        document.getElementById("alert-noTelp").innerHTML = "Harap masukan nomor telepon anda"
        noTelp.focus();
        return false;
    }
    else{
        document.getElementById("alert-noTelp").innerHTML ="";
    }
    if(email.value.length == 0){
        document.getElementById("alert-email").innerHTML = "Harap masukan email anda"
        email.focus();
        return false;
    }
    else{
        document.getElementById("alert-email").innerHTML ="";
    }
    if(kabupaten.value.length == 0){
        document.getElementById("alert-kabupaten").innerHTML = "Harap masukan kabupaten anda"
        kabupaten.focus();
        return false;
    }
    else{
        document.getElementById("alert-kabupaten").innerHTML ="";
    }
    if(provinsi.value.length == 0){
        document.getElementById("alert-provinsi").innerHTML = "Harap masukan provinsi anda"
        provinsi.focus();
        return false;
    }
    else{
        document.getElementById("alert-provinsi").innerHTML ="";
    }
    if(alamat.value.length == 0){
        document.getElementById("alert-alamat").innerHTML = "Harap masukan alamat anda"
        alamat.focus();
        return false;
    }
    else{
        document.getElementById("alert-alamat").innerHTML ="";
    }
}
