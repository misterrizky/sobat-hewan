function validasi1(){
    var passWord = document.getElementById("input-password");
    var userName = document.getElementById("input-name");
    if(userName.value.length == 0){
        document.getElementById("alert-input-name").innerHTML = "Username tidak boleh kosong."
        userName.focus();
        return false;
    }
    else{
        document.getElementById("alert-input-name").innerHTML = "";
    }
    if(passWord.value.length == 0){
        document.getElementById("alert-input-password").innerHTML = "Password tidak boleh kosong."
        passWord.focus();
        return false;
    }
    else{
        document.getElementById("alert-input-password").innerHTML ="";
    }
}

$(function () {

    if (localStorage.chkbox && localStorage.chkbox != '') {
        $('#rememberChkBox').attr('checked', 'checked');
        $('#input-name').val(localStorage.username);
        $('#input-password').val(localStorage.pass);
    } else {
        $('#rememberChkBox').removeAttr('checked');
        $('#input-name').val('');
        $('#input-password').val('');
    }

    $('#rememberChkBox').click(function () {

        if ($('#rememberChkBox').is(':checked')) {
            // save username and password
            localStorage.username = $('#input-name').val();
            localStorage.pass = $('#input-password').val();
            localStorage.chkbox = $('#rememberChkBox').val();
        } else {
            localStorage.username = '';
            localStorage.pass = '';
            localStorage.chkbox = '';
        }
    });
});