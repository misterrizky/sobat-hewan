function validasi(){
    var checkRadio = document.querySelector('input[name="radio1"]:checked');
    if(checkRadio == null) {
        document.getElementById("alert-check-radio1").innerHTML = "Silahkan pilih salah satu pilihan yang ada"; 
        return false;
    } 
    else { 
        document.getElementById("alert-check-radio1").innerHTML = "";
    }

    var checks = document.getElementsByName('check1');
    var hasChecked = 0;
    for(var i = 0; i < checks.length; i++){
        if (checks[i].checked){
            hasChecked++;
        }
    }
    if (hasChecked < 1){
        document.getElementById("alert-checkbox1").innerHTML = "Checkbox tidak boleh kosong.";
        return false;
    }
    else{
        document.getElementById("alert-checkbox1").innerHTML = "";
    }

    var AlasanAdopsi = document.getElementById("alasan-adopsi");
    if(AlasanAdopsi.value == ''){
        document.getElementById("alert-alasan-adopsi").innerHTML = "Alasan adopsi tidak boleh kosong.";
        AlasanAdopsi.focus();
        return false;
    }
    else{
        document.getElementById("alert-alasan-adopsi").innerHTML = "";
    }
}