var komentar = [ ];

function tampilkanKomentar() {
  var kolomKomentar = document.getElementById("commentContent");
  kolomKomentar.innerHTML = "<h4 class='title-comment-section'>Komentar oleh pengguna</h4><p class='subtitle-comment-section'>lihat komentar yang kamu ketikkan di bawah</p>";
  for (i = 0; i < komentar.length; i++) {
    var btnEdit = "<button class = 'btn-edit' href='#' onClick='editKomentar(" + i + ")'>Edit</button>";
    var btnHapus = "<button class = 'btn-hapus' href='#' onclick='hapusKomentar(" + i + ")'>Hapus</button>";
    kolomKomentar.innerHTML += "<div class='container-comment-section'><p class='isi-comment-section'>" + komentar[i].deskripsiKomentar + "</p>" + "<p class='nama-isi-comment-section'>Komentar ditulis oleh " + komentar[i].tulisKomentar + "</p>" + btnEdit + " " + btnHapus + "<br/></div><br/>";
  }
}

function tambahKomentar() {
    var isiKomentar = document.getElementById("isi-komen");
    var isi = document.getElementById("isi-komen").value;
    var namaKomentator = document.getElementById("nama-komen");
    var nama = document.getElementById("nama-komen").value;
    if(isiKomentar.value.length == 0){
        alert("Mohon isi field jika kamu ingin berkomentar");
        isiKomentar.focus();
        return false;
    }
    else if(namaKomentator.value.length == 0){
      alert("Mohon isi nama kamu jika ingin berkomentar");
      namaKomentator.focus();
      return false;
    }
    else{
      var komentarOrang = {
          deskripsiKomentar:isi,
          tulisKomentar:nama
      }
      komentar.push(komentarOrang);
      console.log(komentarOrang);
    tampilkanKomentar();
    }
}

function hapusKomentar(id) {
  komentar.splice(id, 1);
  tampilkanKomentar();
}

function editKomentar(id) {
  var newkomentar = prompt("Silahkan ubah komentar Anda", komentar[id].deskripsiKomentar);
  komentar[id].deskripsiKomentar = newkomentar;
  tampilkanKomentar();
}