<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Exception;

class AuthController extends Controller
{
    public function index()
    {
        
    }
    public function do_login(Request $request)
    {
        // Validator::extend('username', function($attr, $value){
        //     return preg_match('/^\S*$/u', $value);
        // });
        try {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|min:8'
            ]);
        }catch (Exception $e) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Sorry, looks like there are some errors detected, please try again.',
            ]);
        }
        // if(Auth::guard('employee')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember))
        // {
            return response()->json([
                'alert' => 'success',
                'message' => 'Welcome back Sobat Hewan',
                // 'message' => 'Welcome back '. Auth::guard('employee')->user()->name,
            ]);
        // }
    }
    public function do_register(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|min:8'
            ]);
        }catch (Exception $e) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Sorry, looks like there are some errors detected, please try again.',
            ]);
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Welcome back Sobat Hewan',
            // 'message' => 'Welcome back '. Auth::guard('employee')->user()->name,
        ]);
    }
    public function do_forgot(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email'
            ]);
        }catch (Exception $e) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Sorry, looks like there are some errors detected, please try again.',
            ]);
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Welcome back Sobat Hewan',
            // 'message' => 'Welcome back '. Auth::guard('employee')->user()->name,
        ]);
    }
}
