<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="landingPage.html">
            <span class="brand-name">SobatHewan</span>
            <img src="assets/images/web-logo.png" height="30" px class="d-inline-block align-top"
                alt="landingPage.html">
        </a>
        <ul>
            <li><a href="daftarhewan.html">Daftar Hewan</a></li>
            <li><a href="donasiPage.html">Donasi</a></li>
            <li>
                <div class="dropdown show">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="assets/images/logo-user.png" width="20" px />
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="loginLandingPage.html">Logout</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>