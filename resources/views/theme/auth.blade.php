<!DOCTYPE html>
<html lang="en">
@include('theme.auth_head')
<body>
    @yield('content')
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/toastr.js')}}"></script>
    <script src="{{asset('js/plugin.js')}}"></script>
    <script type="text/javascript">
        $("body").on("contextmenu", "img", function(e) {
            return false;
        });
        $('img').attr('draggable', false);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('custom_js')
</body>
</html>