<!DOCTYPE html>
<html lang="en">
@include('theme.head')
<body>
    <div>
        @include('theme.nav')
        @yield('content')
        <div class="mb-5"></div>
        @include('theme.footer')
    </div>
    @include('theme.script')
    @yield('custom_js')
</body>
</html>