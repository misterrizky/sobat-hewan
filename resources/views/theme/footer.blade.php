<div class="jumbotron footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 footer-left">
                <h6 class="footer-title">Contact Us</h6><br />
                <p class="footer-subtitle">+62 811 5338 123</p>
                <p class="footer-subtitle">sobathewan@gmail.com</a>
                    <p class="footer-subtitle">12, Petisah, Medan, Indonesia</p>
            </div>
            <div class="col-md-6 footer-right">
                <h6 class="footer-title">SobatHewan<img src="/assets/images/web-logo.png" alt="" width="30px">
                </h6>
                <div class="social-media">
                    <a href="https://facebook.com" class="link-to-social-media">
                        <img src="assets/images/socialmedia/facebook-logo.png" width="60" px />
                    </a>
                    <a href="https://instagram.com" class="link-to-social-media">
                        <img src="assets/images/socialmedia/instagram-logo.png" width="60" px />
                    </a>
                    <a href="https://twitter.com" class="link-to-social-media">
                        <img src="assets/images/socialmedia/twitter-logo.png" width="60" px />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>