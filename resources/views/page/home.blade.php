@extends('theme.main')
@section('content')
<div class="jumbotron">
    <div class="container">
        <div class="row">
            <h5 class="opening-jumbotron-text">
                <h1>Hai Kevin Selamat datang di SobatHewan!</h1>
            </h5>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Adopsi</h5>
                        <p class="card-text">Disini kami menyediakan hewan-hewan terlantar yang bisa kamu
                            adopsi. Silahkan klik tombol lihat untuk melihat daftar hewan</p>
                        <button><a href="#">Lihat</a></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Donasi</h5>

                        <p class="card-text">Kamu dapat mendonasikan hewan peliharaan mu. Silahkan klik tombol
                            donasi untuk berdonasi.</p>
                        <button><a href="#">Donasi</a></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-12">
             <img src="Line 6.png" alt="" width="100%"  style="display: inline-block;">
            </div>
        </div>
        <div class="row mt-3"> 
            <div class="col-md-8">
                <h2>Pilih Hewan yang ingin kamu adopsi <span><img src="Group 28.png" alt=""></span></h2>
               </div>
               <div class="col-md-4">
                <button><a href="#">Kucing</a></button>
                <button><a href="#">Anjing</a></button>
               </div>
        </div>
    </div>
</div>
@endsection