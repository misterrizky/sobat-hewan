@extends('theme.auth')
@section('content')
<section>
    <div class="contentBx">
        <div class="formBx" id="login_page">
            <div class="logo">
                <img src="{{asset('images/icon.png')}}" alt="">
            </div>
            <h2>Login</h2>
            <p>Silahkan login ke akun anda yang terdaftar.</p>
            <form id="form_login" autocomplete="off">
                <div class="inputBx">
                    <span>Email*</span>
                    <input type="email" name="email">
                </div>
                <div class="inputBx">
                    <span>Password*</span>
                    <input type="password" name="password">
                </div>
                <div class="remember">
                    <label><input type="checkbox"> Ingat saya!</label>
                </div>
                <div class="forget">
                    <a href="javascript:void(0);" onclick="show_hide_form('#forgot_page','#login_page','#register_page');">Lupa Password?</a> 
                </div>
                <div class="inputBx">
                    <input id="tombol_login" onclick="auth('#form_login','#tombol_login','login','Login');" type="button" value="Login">
                </div>
                <div class="inputBx">
                    <p>Belum terdaftar?
                        <a href="javascript:void(0);" onclick="show_hide_form('#register_page','#login_page','#forgot_page');"> Daftar disini</a>
                    </p>
                </div>
            </form>
        </div>
        <div class="formBx" id="register_page">
            <div class="logo">
                <img src="{{asset('images/icon.png')}}" alt="">
            </div>
            <h2>Register</h2>
            <p>Silahkan login ke akun anda yang terdaftar.</p>
            <form id="form_register" autocomplete="off">
                <div class="inputBx">
                    <span>Email*</span>
                    <input type="text" name="email">
                </div>
                <div class="inputBx">
                    <span>Password*</span>
                    <input type="password" name="password">
                </div>
                <div class="inputBx">
                    <input id="tombol_register" onclick="auth('#form_register','#tombol_register','register','Register');" type="button" value="Register">
                </div>
                <div class="inputBx">
                    <p>Sudah terdaftar?
                        <a href="javascript:void(0);" onclick="show_hide_form('#login_page','#register_page','#register_page');"> Login disini</a>
                    </p>
                </div>
            </form>
        </div>
        <div class="formBx" id="forgot_page">
            <div class="logo">
                <img src="{{asset('images/icon.png')}}" alt="">
            </div>
            <h2>Forgot</h2>
            <p>Silahkan login ke akun anda yang terdaftar.</p>
            <form id="form_forgot" autocomplete="off">
                <div class="inputBx">
                    <span>Email*</span>
                    <input type="email" name="email">
                </div>
                <div class="inputBx">
                    <input id="tombol_forgot" onclick="auth('#form_forgot','#tombol_forgot','forgot','Forgot');" type="button" value="Forgot">
                </div>
                <div class="inputBx">
                    <p>Ingat akun anda?
                        <a href="javascript:void(0);" onclick="show_hide_form('#login_page','#forgot_page','#register_page');"> Login disini</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
    <div class="imgBx">
        <img src="{{asset('images/dog.jpg')}}">
    </div>
</section>
@endsection
@section('custom_js')
    <script type="text/javascript">
        $("#login_page").show();
        $("#register_page").hide();
        $("#forgot_page").hide();

        function show_hide_form(show, hide, hides)
        {
            $(show).show();
            $(hide).hide();
            $(hides).hide();
        }
        function auth(form,button,uri,title)
        {
            $(button).submit(function () {
                return false;
            });
            let data = $(form).serialize();
            $(button).prop("disabled", true);
            $(button).val('Please Wait');
            $.ajax({
                type: "POST",
                url: uri,
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response.alert=="success") {
                        success_message(response.message);
                        $(form)[0].reset();
                        setTimeout(function () {
                            $(button).prop("disabled", false);
                            $(button).val(title);
                            if(title == "Login"){
                                location.href = "home";
                            }else if(title == "Register"){
                                show_hide_form('#login_page','#register_page','#forgot_page');
                            }else if(title == "Forgot"){
                                show_hide_form('#login_page','#register_page','#forgot_page');
                            }
                        }, 2000);
                    } else {
                        error_message(response.message);
                        setTimeout(function () {
                            $(button).prop("disabled", false);
                            $(button).val(title);
                        }, 2000);
                    }
                },
            });
        }
    </script>
@endsection