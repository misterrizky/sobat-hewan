<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.main');
});
Route::get('home', function () {
    return view('page.home');
});
Route::post('login','AuthController@do_login')->name('login');
Route::post('register','AuthController@do_register')->name('register');
Route::post('forgot','AuthController@do_forgot')->name('forgot');